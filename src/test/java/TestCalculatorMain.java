import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import java.util.Arrays;
import java.util.List;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class TestCalculatorMain {

        private CalculatorMain calculatorMain;

        @BeforeEach
        @Tag("development")
        public void initACalculatorBeforeEach(){
            calculatorMain = new CalculatorMain();
        }
        @AfterEach
        @Tag("development")
        public void clearACalculatorBeforeEach(){
            calculatorMain = null;
        }

        // £= Låt samtliga test kontrollera så eventuella Exceptions också tas om
        //hand.
        // $=  Kontrollera ordningen på hur tester körs.

        @ParameterizedTest                                              //Låt en metod använda @ParameterizedTest.
        @Order(1) //$
        @Tag("development")
        @DisplayName("This method add sebbe==38835 to any number")  // Namnge minst ett test.
        @ValueSource(ints = {1, 0, 5, -3, 15, 20000})
        void testAddSebbeToNumber(int ints){

            int sebbe = 38835;
            int sum = sebbe + ints;
            Assertions.assertEquals(sum, calculatorMain.addSebbeToNumber(ints));
            Assertions.assertDoesNotThrow(()->calculatorMain.addSebbeToNumber(ints));                       //£
        }

        @Test
        @Order(2)
        @Tag("development")
        @DisplayName("Test arrays lenght")
        void testCheckArrayLenght() {                            // • Låt en metod testa en array’s storlek.

            int[] testArray = new int[11];
            Assertions.assertEquals(11, calculatorMain.checkArrayLenght(testArray));
            assertDoesNotThrow(()->calculatorMain.checkArrayLenght(testArray));                             //£
        }

        @Test
        @Order(3)
        @Tag("development")
        @DisplayName("Division method with exeption for Zero-division error")
        void testDivideByZeroError () {

           Assertions.assertEquals(5, calculatorMain.divideByZeroTestning(15,3));
           assertThrows(ArithmeticException.class, () -> {                                                  //£
               calculatorMain.divideByZeroTestning(15, 0);});
        }

        @Test
        @Order(4)
        @Tag("development")
        @DisplayName("Test private method, conveumberFormatExceptionrt. String to number, gives exception if fail")
        void testStringToNummerNotPrivate() {               // Minst en metod ska vara private och säkerställas den testas.

            String tio = "10";
            String tolv = "tolv";

            Assertions.assertEquals(10, calculatorMain.stringToNummerNotPrivate(tio));
            assertThrows(NumberFormatException.class, () -> {                                                          //£
                calculatorMain.stringToNummerNotPrivate(tolv);});
        }

        @Test
        @Order(5) //$
        @Tag("development")
        @DisplayName("Find a word in list ")  // Namnge minst ett test.
        void testStringStream(){

            List<String> test = Arrays.asList("one", "two", "three");
            String rattord = "two";
            Assertions.assertTrue(calculatorMain.checkWordInArray(rattord, test));
        }

        @Test
        @Order(6) //$
        @Tag("development")
        @DisplayName("Return random number 1-6, dice simulator, test that null is not returned")  // Namnge minst ett test.
        void testReturnRandomNumber(){
                Assertions.assertNotNull(calculatorMain.returnRandomNumber());
        }

    }







